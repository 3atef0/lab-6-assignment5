package com.example.lab6_assignment5;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.example.lab5hands_onassignment4.R;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    Button buttonSearch;
   // Button buttonAdd;
    EditText editTextSearch ;
    ListView listView_SearchRes ;
    ArrayList<String> searchRes ;
    ArrayList<Emplyee> searchResEmpList ;
    DBHelper dbHelper;
    ArrayAdapter<String> arrayAdapter;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        buttonSearch = findViewById(R.id.butt_search);
      //  buttonAdd = findViewById(R.id.butt_add);
        editTextSearch = findViewById(R.id.eT_Search);
        listView_SearchRes = findViewById(R.id.listView_SearchRes);
        searchRes = new ArrayList<>();
        searchResEmpList = new ArrayList<>();
        arrayAdapter = new ArrayAdapter<>(this,android.R.layout.simple_list_item_1, android.R.id.text1,searchRes);
        listView_SearchRes.setAdapter(arrayAdapter);
        listView_SearchRes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {

                Intent intent = new Intent(getBaseContext(), EmpInfoActivity.class);
                intent.putExtra("EMPNAME", searchResEmpList.get(i).name);
                intent.putExtra("EMPEMAIL", searchResEmpList.get(i).email);
                intent.putExtra("EMPPHONE", ""+searchResEmpList.get(i).phone);
                intent.putExtra("EMPTITLE", searchResEmpList.get(i).title);
                String deptName = dbHelper.getDepartmentName(searchResEmpList.get(i).dept_id);
                intent.putExtra("DEPTNAME", deptName);
                startActivity(intent);
            }
        });
        dbHelper =new DBHelper(this);
        buttonSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(editTextSearch.getText().toString().isEmpty()){
                    clearList();
                }else{
                    searchResEmpList =  dbHelper.fetchAllEmpNameWhere(editTextSearch.getText().toString())    ;
                    searchRes.clear();
                    for(int i=0;i<searchResEmpList.size();i++){
                        searchRes.add(searchResEmpList.get(i).name);
                    }
                    arrayAdapter.notifyDataSetChanged();
                }
            }
        });
/*        buttonAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(editTextSearch.getText().toString().isEmpty()){
                    Toast.makeText(MainActivity.this, "INVALID", Toast.LENGTH_SHORT).show();
                }else{
                    dbHelper.createNewEmp("dummy",editTextSearch.getText().toString(),"dummy",123,"dummy" ,true);
                    clearList();
                }
            }
        });*/
    }
    public void clearList(){
        searchRes.clear();
        arrayAdapter.notifyDataSetChanged();
    }
}

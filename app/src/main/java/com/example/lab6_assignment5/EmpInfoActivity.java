package com.example.lab6_assignment5;

import androidx.appcompat.app.AppCompatActivity;

import android.annotation.SuppressLint;
import android.os.Bundle;
import android.widget.TextView;

import com.example.lab5hands_onassignment4.R;

public class EmpInfoActivity extends AppCompatActivity {
    TextView textViewName;
    TextView textViewTitle;
    TextView textViewPhone;
    TextView textViewEmail;
    TextView textViewDept;
    @SuppressLint("SetTextI18n")
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emp_info);
        textViewName=findViewById(R.id.textViewName);
        textViewTitle=findViewById(R.id.textViewTitle);
        textViewPhone=findViewById(R.id.textViewPhone);
        textViewEmail=findViewById(R.id.textViewEmail);
        textViewDept=findViewById(R.id.textViewDept);
        textViewName.setText("Name : "+getIntent().getStringExtra("EMPNAME"));
        textViewEmail.setText("Email :"+getIntent().getStringExtra("EMPEMAIL"));
        textViewPhone.setText("phone : "+getIntent().getStringExtra("EMPPHONE"));
        textViewTitle.setText("Title : "+getIntent().getStringExtra("EMPTITLE"));
        textViewDept.setText("Department : "+getIntent().getStringExtra("DEPTNAME"));
       // textViewDept
    }
}
